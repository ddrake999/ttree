#!/usr/bin/env ruby
require_relative './truth_tree'
require_relative './branch'
require_relative './formula'
require 'pp'

# A Utility for generating a sample set of rules, such as in samples/generated1.txt

cur_path = File.expand_path File.dirname(__FILE__)
if ARGV.count != 1
  puts "Please specify a clues file to process"
else
  filename = ARGV[0]
  filepath = File.expand_path File.join(cur_path, filename) 
  contents = File.open(filepath, 'r') { |f| f.read } 
  clues = contents.strip.split("\n")
  all_stmts = clues.dup
  clues.each do |c|
    all_stmts << "# #{c}"
    crow, ccol, cval = c.split("_").map { |comp| comp.to_i }
    cbox = ((crow-1)/3)*3 + (ccol-1)/3
    all_stmts |= (1..9).map { |val| val == cval ? nil : "~#{crow}_#{ccol}_#{val}" }.compact!
    all_stmts |= (1..9).map { |col| col == ccol ? nil : "~#{crow}_#{col}_#{cval}" }.compact!
    all_stmts |= (1..9).map { |row| row == crow ? nil : "~#{row}_#{ccol}_#{cval}" }.compact!
    temp = []
    for box_row in 1..3
      for box_col in 1..3
        row = (cbox/3)*3 + box_row
        col = (cbox%3)*3 + box_col
        temp << "~#{row}_#{col}_#{cval}" unless row == crow && col == ccol
      end
    end
    all_stmts |= temp
  end
  # subtract clues.count because of the comment lines we're putting in...
  atoms_from_clues = all_stmts.count - clues.count
  puts "# Atoms from clues: #{atoms_from_clues}"
  puts "# Just need to identify #{9*9*9 - atoms_from_clues} more!"

  # For any cell, the value must be in 1..9
  temp = []
  for col in 1..9
    for row in 1..9
      atoms = []
      for val in 1..9
        atoms << "#{row}_#{col}_#{val}"
      end
      temp << atoms.join("|")
    end 
  end
  all_stmts |= temp

  # Each number must appear in any given row
  temp = []
  for row in 1..9
    for val in 1..9
      atoms = []
      for col in 1..9
        atoms << "#{row}_#{col}_#{val}"
      end
      temp << atoms.join("|")
    end 
  end
  all_stmts |= temp

  # Each number must appear in any given column
  temp = []
  for col in 1..9
    for val in 1..9
      atoms = []
      for row in 1..9
        atoms << "#{row}_#{col}_#{val}"
      end
      temp << atoms.join("|")
    end 
  end
  all_stmts |= temp

  # Each number must appear in any given box
  temp = []
  for box in 0..8
    for val in 1..9
      atoms = []
      for box_row in 1..3
        for box_col in 1..3
          row = (box/3)*3 + box_row
          col = (box%3)*3 + box_col
            atoms << "#{row}_#{col}_#{val}"
        end
      end
      temp << atoms.join("|")
    end 
  end
  all_stmts |= temp

  # A given number may not appear more than once in any given row
  temp = []
  for row in 1..9
    for val in 1..9
      for key_col in 1..9
        for oth_col in 1..9
          temp << "~#{row}_#{key_col}_#{val} | ~#{row}_#{oth_col}_#{val}" unless oth_col == key_col
        end
      end
    end 
  end
  all_stmts |= temp

  # A given number may not appear more than once in any given column
  temp = []
  for col in 1..9
    for val in 1..9
      for key_row in 1..9
        for oth_row in 1..9
          temp << "~#{key_row}_#{col}_#{val} | ~#{oth_row}_#{col}_#{val}" unless oth_row == key_row
        end
      end
    end 
  end
  all_stmts |= temp

  # A given number may not appear more than once in any given box
  temp = []
  for box in 0..8
    brow = (box/3)*3
    bcol = (box%3)*3
    for val in 1..9
      for box_row in 1..3
        for box_col in 1..3
          key_row = brow + box_row
          key_col = bcol + box_col
          for oth_box_row in 1..3
            for oth_box_col in 1..3
              oth_row = brow + oth_box_row
              oth_col = bcol + oth_box_col
              temp << "~#{key_row}_#{key_col}_#{val} | ~#{oth_row}_#{oth_col}_#{val}" unless oth_row == key_row && oth_col == key_col 
            end
          end
        end
      end
    end 
  end
  all_stmts |= temp

  # subtract clues.count because of the comment lines we're putting in...
  puts "# Total of clues and rules: #{all_stmts.count - clues.count}"
  puts all_stmts.join("\n")
end
