ttree: A Truth Tree Processor 
=============================
(for Analyzing a Set of Logical Expressions)
=============================================

To Use for processing a file containing expressions
---------------------------------------------------

- Install Ruby Version 1.93 or higher
- Clone this repository or download and extract the files.
- Ensure that the scripts are executable. `chmod +x ttree.rb ttri.rb britgen.rb sudogen.rb`

To process a file containing expressions
----------------------------------------

`./ttree.rb samples/example1.txt`

use `./ttree.rb` for a list of options

To Use interactively
--------------------

`./ttri.rb`

Expression Syntax
-----------------

Expressions must satisfy the following rules to be considered valid:

- Sentence 'letters' may be of any length, but must consist of word characters (a-zA-Z0-9_)
- Negation is currently represented by ~ , but can be input as ! if desired
- Logical 'And' is currently represented by & , but can be input as ^ if desired
- Logical 'Or' is currently represented by | 
- Right Arrow is currently represented by -> (hyphen arrow)
- Double Arrow is currently represented by <-> 
- Only '(' and ')' are used as parentheses, but input can contain { } and [ ] as well
- An expression may include spaces or tabs; they will be ignored.
- No precedence rules are assumed for any logical operators, so parentheses are required in expressions with different operators.  Examples (p&q)|r or p&(q|r)
- Parentheses are required for repeated use of the same operator, except for & and |.  Examples: a&b&c is alowed. p->q->r is not. p->(q->r) is.  (p->q)->r is too.

Input Format
------------

- Multiple expressions can be input with each expression on its own line.
- Comments may be included by putting a # at the beginning of a line.
- Blank lines will be ignored.

Features
--------
This implementation has a few features which may be unusual for truth tree algorithms, but can be quite helpful:

- The 'And' and 'Or' operators allow any number of operands.  
- A recursive method for selecting the next formula allows checking the grandchildren of an expression for branch closing capability as well as the immediate children.  This could be extended quite easily to great-grandchildren.  Of course there is a performance trade off -- this sort of optimization could either improve or degrade performance depending on the set being analyzed.
- By default, when decomposing a formula, if two or more of the newly created nodes duplicate existing nodes in the formula's set of branches, the algorithm closes one or more of these new branches, since they do not add any new information. When a branch is closed for duplication, the letter "D" and the line number where the duplicate appears are appended to the X marking the branch as closed.  This feature tends to reduce branching for some problems, making the tree is easier to follow.  Also, it tends to reduce duplication of models when they exist.  However, it is important to realize that this technique can only be applied to a decomposition step if at least one open sub-branch of the original formula will remain.  To see why this restriction is necessary, consider that otherwise the set p|q, p&q would be closed.

Options
-------

Several options are available for processing files with ttree.rb.  The current options can be listed by entering `./ttree.rb` at a command prompt without any arguments.

- -d : Suppress tree display
- -s : Suppress all display (tree and summary)
- -t : Display timing for some long-running processes
- -f : Don't allow 'picking the first' as a formula selection method. It tends to produce lots of branches resulting in very long run times for large problems.  If this is specified, the tree will never have more than 1 open branch.
- -g : Don't check for closing grandchildren expressions.  The algorithm for this can perform badly for large problems.  For many large problems, such as British-style logic puzzles, it's not difficult to massage a clue into a set of conditionals.
- -p : Stop processing when the atom count is reached.  This may not result in a true model, but can be helpful if redundant rules are given
- -a : Expected atom count. If specified, an error will be raised if this value doesn't match the calculated count
- -u : Do not close branches due to duplicates. The default is to close a newly created branch if the last expression is already in the branch and at least one open branch will remain for the original formula.  It may be useful to specify this option for performance reasons.


Output Format
-------------

Currently the output format is not beautiful, but gets the job done.  The output can be suppressed entirely (e.g. for running specs) or set to print only the summary.  Here are some short examples:
Closed Branches:

    Processing 'samples/example2.txt' with contents:

    p&q
    ~p
    
    ----------------------------------------
    
    Line 1.   Br. 1     Set Member
              (p&q) 
                
    
    Line 2.   Br. 1     Set Member
               ~p   
                    
    
    Line 3.   Br. 1     1. &
                p   
                    
    
    Line 4.   Br. 1     1. &
                q   
                x   
    
    
    Tree is complete.
    No model could be found.
    
    The last step taken was With Non-branching expression. Expression: (p&q) in line 4.
    Run time: 0.000404783
    ----------------------------------------

Open branch:

    Processing 'samples/example3.txt' with contents:
    
    p|q
    ~p
    
    ----------------------------------------
    
    Line 1.   Br. 1     Set Member
              (p|q) 
                    
    
    Line 2.   Br. 1     Set Member
               ~p   
                    
    
    Line 3.   Br. 1  Br. 2     1. |
                p      q   
                x          
    
    
    Tree is complete.
    Model(s) were found:
    Branch 2:  p = false, q = true
    The following atoms are true:
    q
    
    
    The last step taken was With Closing Subexpressions. Expression: (p|q) in line 3.
    Run time: 0.000456982
    ----------------------------------------


Any expression that is associated with a line is noted below the branch along with the line whose expression was used to generate it and the decomposition technique.  Any branches not affected by the operation for a given line number will display '|' instead of an expression.  

At the end of the output, a summary of the status of the tree is given:  This will be something like one of the following:

- Tree is complete.  A model was found on branch 3:  p = true, q = false.
  The following atoms are true: p
- Tree is not yet completely processed
- Tree is complete.  No model could be found

Code Structure
--------------

The code is not particularly beautiful, but an attempt was made to make it somewhat legible.  All the code for generating and displaying a truth tree is encapsulated in three classes:

- truth_tree.rb - Represents a truth tree.  
- branch.rb - Represents a branch of a truth tree
- formula.rb - Represents a logical formula

Please refer to the comments in each class for more information.

Utilities
---------

Two utilities are provided for generating sample rules.  britgen.rb generates rules for "British-style" logic puzzles, and sudogen.rb generates rules for sudoku puzzles.  britgen.rb requires a 'definitions' file to generate the sentence letters.  sudogen requires a 'clues' file and uses it to generate some 'expanded clues' for a normal 9x9 sudoku.

A few sample files are provided, which combine the standard rules with 'clues' for specific puzzles.  This algorithm, which is primarily designed to provide legible code and output, is very inefficient on these types of problems.  For example, a typical
British-style puzzle with 4 categories and 4 options for each category takes about 20 seconds to run on my machine.  One with 4 categories and 5 options for each category takes about 1 minute.  A sudoku (easy or 'evil' makes no difference) takes about 8 hours or so -- it has around 15,000 formulas in the main branch when it starts, so decomposition is very expensive.  These large problems are mainly interesting for the purposes of understanding trade-offs and testing various optimizations.

Goals
-----

- Add more test specs.
- Extend the algorithm to handle predicate logic
- Add threading
- More refactoring: Possibly move some methods from Formula into a new 'Atom' class.  Make the code as easy to read as possible.

Credits
-------

Thanks to Prof. D Weber at PSU for graciously allowing me to sit in on his Intro to Formal Logic course.  It's been very enlightening.
