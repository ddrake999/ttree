# A branch stores an (ordered) array of formulas.
# A branch maintains a reference to the truth tree of which it is a member.
# A branch is open or closed and can report this information.
# A branch is either fully_decomposed or not and can report this information.
# A branch has a method 'add_formulas' that appends an array of formulas to its array and updates the 
# A branches array and branch_indices array for the expression
class Branch

  attr_reader :formulas, :atoms, :truth_tree, :created_in_line, :closing_line, :parent_branch, 
    :duplicates_line, :dup_closing_line

  def initialize(truth_tree, parent_branch)
    # @atoms is a hash where the key is a sentence letter and the value is one of 
    # the following: true, false, :contradiction 
    @atoms = {} 
    @formulas = []
    @truth_tree = truth_tree
    @parent_branch = parent_branch
    @closed = false
    @decomposed = false
    @created_in_line = (truth_tree.last_line || 0) + 1
    @duplicates_line
  end

  def index_in_branch_orders(line)
    branch_orders = truth_tree.get_branch_orders_for_line line
    branch_orders.index(index_in_tree)
  end

  def index_in_tree
    truth_tree.branches.index(self)
  end

  def open_complete?
    !closed? && complete?
    # note !closed? is first operand, so we utilize caching.
  end

  def model_text(force=false)
    return 'N/A' unless open_complete? || force
    self.class.model_text(@atoms)
  end

  def true_atoms_text
    self.class.true_atoms_text(@atoms)
  end

  def self.model_text(atoms)
    all_atoms = atoms.sort.map{ |ar| "#{ar[0]} = #{ar[1]}" }.join(', ')
    "#{all_atoms}\n#{true_atoms_text(atoms)}\n"
  end

  def self.true_atoms_text(atoms)
    true_atoms = atoms.select{ |k, v| v == true }.map { |k, v| k }.sort
    "The following #{true_atoms.count} atoms are true:\n#{true_atoms.join(', ')}"
  end

  def closed?
    @closed
  end

  # check each atomic formula for a contradiction (sentence letter and its negation)
  def try_close(formula)
    if @atoms.detect { |k, v| v == :contradiction }
      @closed = true
      @closing_line = formula.line
      @duplicates_line = nil
    end
  end

  def decomposed?
    @decomposed ||= @formulas.inject(true) { |all_decomposed, f| all_decomposed &&  f.decomposed? }
  end

  def non_decomposed_formulas
    @formulas.select { |f| !f.decomposed? }
  end

  def last_line
    formulas[-1].line
  end

  # a branch is complete if either it is closed (contradiction reached) or fully decomposed
  # or if the option is set to stop when the total atoms count has been reached and it has been.
  def complete?
    closed? || decomposed? || (truth_tree.stop_at_atom_count && @truth_tree.total_atoms == @atoms.count)
  end

  def status
    closed? ? "Closed (COMPLETE)" : "Open" + (decomposed? ? " (COMPLETE)" : "")
  end

  def atoms_array
    @atoms.map { |letter, negated| (negated ? "~" : "") + "#{letter}" }
  end

  def add_formulas(formula_arr)
    formula_arr.each do |formula|
      is_atom = formula.atomic?
      formula.add_branch(self)
      @formulas << formula
      update_atoms(formula.expression) if is_atom
      try_close formula if !@closed
      if !@closed && truth_tree.close_due_to_duplicates
        @duplicates_line = formula_duplicates_line(formula) 
        @dup_closing_line = formula.line if @duplicates_line
      end
    end
  end

  def formula_duplicates_line(formula)
    existing = formulas.detect { |f| f != formula && f.expression == formula.expression }
    existing && existing.line
  end

  # Note: returns false if this atom is closing
  def atom_duplicates_line(letter, negated)
    !closing_atom?(letter, negated) && formulas.detect { |f| f.expression == ((negated ? '~' : '') + letter) } 
  end

  def close_due_to_duplicates
    @closed = true
    @closing_line = @dup_closing_line
    @dup_closing_line = nil
  end

  def update_atoms(atom)
    letter, negated = Formula.atom_attributes(atom)
    test = @atoms[letter]
    return if (test == :contradiction) || (test == true && !negated) || (test == false && negated)
    if (test == true && negated) || (test == false && !negated)
      @atoms[letter] = :contradiction
    else 
      @atoms[letter] = !negated
    end
  end

  # Return true if the atom provided will close this branch, otherwise false.
  def closing_atom?(letter, negated)
    raise "Branch is already closed" if closed?
    (@atoms[letter] == true && negated) || (@atoms[letter] == false && !negated)
  end


end