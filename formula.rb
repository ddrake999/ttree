# A formula knows how to decompose itself into simpler formulas.  It does so as follows:
#   0. parses its expression to determine whether the overall expression is negated, 
#      gets a list of raw sub-expressions and a connecting operator.  
#      If the connecting operator is & or |, any number of sub-expressions are allowed.
#      If the connecting operator is -> or <->, only two sub-expressions are allowed.
#   1. creates a temporary array of new (sub)formulas with appropriate negations and 
#      notes the final operator (AND or OR).
#      Each such new formula will be initialized with its expression, parent_formula and decomp_method
#   2. if the decomposition was non-branching, 
#      appends a reference to each formula to each of its branches in the truth tree and sets the branches 
#      and branch index for each formula accordingly.  
#   3. if the decomposition was branching, 
#      creates n new branches where n is the number of sub-expressions minus 1.
#      it creates the branches by cloning its own branch.
#      it then appends a the first formula to its first branch, and each of the other
#      new formulas to one of the new branches.  Note that <-> and ~<-> append two new formulas to each branch.
# A formula knows whether it is atomic
# A formula contains a reference to its branches (an array)
# A formula knows its line number within the tree.
# A formula holds a reference to its parent formula, unless it is a set member.
# A formula knows whether it has been decomposed.
# If a formula was produced by decomposition, it knows the decomposition method that produced it.
class Formula
  attr_reader :expression, :branches, :line, :expression, :subexpressions, :operator, :line, :parent_formula, :decomp_method


  DECOMPS = { positive_and:           { branching: false },
              positive_or:            { branching: true }, 
              positive_right_arrow:   { branching: true },
              positive_double_arrow:  { branching: true },
              negated_and:            { branching: true },
              negated_or:             { branching: false }, 
              negated_right_arrow:    { branching: false },
              negated_double_arrow:   { branching: true }}

  def initialize(expression, line, parent_formula=nil, decomp_method=nil)
    @expression = expression
    @line = line
    @decomposed = atomic?
    @branches = []
    @parent_formula = parent_formula
    @decomp_method = decomp_method
    @is_negated, @operator, @expression, @subexpressions = parse_expression
    @checked_for_closing_subs = false
    @checked_for_closing_sub_subs = false
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``
  # INTROSPECTION METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``

  def branching?
    DECOMPS[decomp_type][:branching]
  end

  def set_member?
    @parent_formula.nil?
  end

  def decomposed?
    @decomposed
  end

  def atomic?
    @is_atomic ||= self.class.is_atomic(@expression)
  end

  def negated?
    @is_negated
  end

  def atomic_subexpressions
    @subexpressions.select { |exp| self.class.is_atomic(exp) }
  end

  def nonatomic_subexpressions
    @subexpressions.select { |exp| !self.class.is_atomic(exp) }
  end

  # This method catches a case that #has_closing_subexpressions? misses.
  # It checks for closing due to duplicates.
  def test_tree_has_closing_subexpressions?
    # Don't Cache.  REMEMBER: The tree is changing at each step, so a formula that is not closing now,
    # may become closing later!!!
    open_branch_ct = open_branches.count
    test_tree = TruthTree.get_test_tree
    self_dup = self.dup.clear_branches
    open_branches.each do |b|
      new_b = test_tree.add_branch nil
      dups = b.formulas.map { |f| f == self ? self : f.dup.clear_branches }
      idx = dups.index(self)
      dups.insert(idx, self_dup)
      dups.delete_at(idx + 1)
      new_b.add_formulas dups
    end
    # should be ok to set test_tree.last_line = truth_tree.last_line 
    start_line = test_tree.last_line = test_tree.branches.inject(0) { |mx, b| mx = [b.last_line, mx].max }
    test_tree.last_line = self_dup.decompose test_tree.last_line
    is_good = test_tree.open_branches.count <= open_branch_ct
    return is_good if is_good 
    return false if nonatomic_subexpressions.count == 0

    new_formulas = []
    test_tree.branches.each { |b| new_formulas |= b.formulas.select { |f| f.line > start_line && !f.atomic? } }
    new_formulas.each { |f| test_tree.last_line = f.decompose test_tree.last_line }
    is_good = test_tree.open_branches.count <= open_branch_ct
    return is_good
  end

  # The method #test_tree_has_closing_subexpressions? does what this method does and more
  # (it's also capable of closing for duplicates), but this method is much more
  # efficient, since it doesn't have the overhead of creating a temporary test tree.
  def has_closing_subexpressions?
    # Look for a net branch closure of 1 or more.  In other words, 
    # an | could have multiple operands.  It could also result in closing multiple branches.
    # Here we want only formulas whose subexpressions will close at least as many branches as they create
    # I tried adding caching here, but it created a lot of complexities and the performance
    # benefits weren't measurable.  REMEMBER: The tree is changing at each step, so a formula that is not closing now,
    # may become closing later!!!
    brs = open_branches
    br_count = brs.count
    good_sub_count = 0
    processed_subs = []

    atomic_subs = atomic_subexpressions
    atomic_sub_count = atomic_subs.count
    return false if atomic_sub_count == 0

    closing_atoms = []
    atomic_subs.each do |atom|
      # count how many of the atomic subexpressions close ALL current open branches
      # compare this to the number of atomic subexpressions.  If the difference is one or less, we're good.
      letter, negated = self.class.atom_attributes(atom)
      if @operator == '<->'
        if  brs.select { |b| b.closing_atom?(letter, negated) }.count == br_count ||
            brs.select { |b| b.closing_atom?(letter, !negated) }.count == br_count
          good_sub_count += 1 
          closing_atoms << atom
        end
      else
        letter, negated = self.class.get_future_atom_attributes atom, @operator, @is_negated, atomic_subexpressions.index(atom)
        processed_subs << [letter, negated]
        if open_branches.select { |b| b.closing_atom?(letter, negated) }.count == br_count
          good_sub_count += 1 
          closing_atoms << atom
        end
      end
    end
    has_closing_subs = (subexpressions.count - good_sub_count <= 1)

    # It would be nice to delay processing a branching formula if its only going to result in adding a duplicate
    # atom...
    remaining_subs = subexpressions - closing_atoms
    if has_closing_subs && remaining_subs.count == 1
      sub = remaining_subs[0]
      if self.class.is_atomic(sub)
        letter, negated = self.class.atom_attributes(sub)
        if brs.inject(true) { |all_dupes, b| all_dupes && b.atom_duplicates_line(letter, negated) }
          has_closing_subs = false
        end
      end
    end

    return has_closing_subs
  end

  def has_closing_subexpressions_dupes?
    brs = open_branches
    br_count = brs.count
    good_sub_count = 0
    processed_subs = []

    atomic_subs = atomic_subexpressions
    atomic_sub_count = atomic_subs.count
    return false if atomic_sub_count == 0

    # a very special case, but common in large problems.  Close for duplication
    if br_count == 1 && @operator != '<->' && atomic_sub_count == @subexpressions.count
      atomic_subs.each do |atom|
        letter, negated = self.class.atom_attributes(atom)
        letter, negated = self.class.get_future_atom_attributes atom, @operator, @is_negated, atomic_subexpressions.index(atom)
        processed_subs << [letter, negated]
      end
      b = brs[0]
      close_test = processed_subs.select { |letter, negated| b.closing_atom?(letter, negated) }
      dup_test = processed_subs.select { |letter, negated| b.atom_duplicates_line(letter, negated) }
      has_closing_subs = (atomic_sub_count - close_test.count == dup_test.count)
    end
    return has_closing_subs
  end


  # Given a subexpression atom, the operator which will be used for decomposition, a flag whether the parent expression
  # is negated and the index of the subexpression within the subexpressions array, return an atom with the correct sign
  def self.get_future_atom_attributes(atom, operator, negated, index)
    letter, neg = atom_attributes(atom)
    if negated && (operator == '&' || operator == '|'|| (operator == '->' && index == 1))
      neg = !neg
    end
    return letter, neg
  end


  def open_branches
    @branches.select { |b| !b.closed? }
  end

  def truth_tree
    @branches.first.truth_tree
  end

  def clear_branches
    @branches = []
    self
  end

  def self.atom_attributes(atom)
    raise "#{atom} is not an atom" unless self.is_atomic(atom)
    negated = atom.chars.to_a.select { |c| c == '~' }.count % 2 == 1
    rev_idx = atom.reverse.index('~')
    if rev_idx
      first_letter_char = atom.length - atom.reverse.index('~')
    else
      first_letter_char = 0
    end
    letter = atom[first_letter_char..-1]
    return letter, negated
  end

  # Note: we're allowing '~~~p' to be considered 'atomic' 
  # to simplify things in handle_negation.
  # A 'sentence letter' may contain any number of characters
  def self.is_atomic(expr)
    !!expr.match(/\A\~*[\w]+\z/)
  end

  def self.letter?(c)
    !!c.match(/\w/)
  end



  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``
  # DISPLAY METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``

  def display_info
    if @parent_formula
      "#{@parent_formula.line}. #{@decomp_method}"
    else
      'Set Member'
    end
  end


  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``
  # PARSING METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``

  # break an expression into subexpressions, an operator and a negation flag.
  # also return a simplified version of the original expression
  def parse_expression
    self.class.parse(@expression)
  end

  def self.parse(expression)
    expr, is_negated = handle_negation(expression)
    subexpressions, operator = get_subexpressions(expr)
    expr = "(#{expr})" if subexpressions.count > 1
    expr = "~#{expr}" if is_negated
    return is_negated, operator, expr, subexpressions
  end

  # this method should handle any number of negations of wrapped expressions
  # it should return a positive expression and an is_negated flag.
  # We only want to deal with complete wrapped expressions preceded by one or more ~'s
  # the expression return should not have outer parentheses
  def self.handle_negation(expr)
    is_negated = false
    s = expr.dup
    # cancel double negations
    s.gsub!(/~~/,'')
    s = remove_outer_parens(s)
    if s[0] == '~' && (s[1] == '~' || wrapped?(s[1..-1]))
      is_negated = true
      s = s[1..-1]
      s, neg = handle_negation(s)
      is_negated = (neg ? !is_negated : is_negated)
      return s, is_negated
    else
      return s, false
    end
  end

  # if more than two subexpressions are provided, they must use the same operator.
  # this method should scan the expression for & or | that are not inside parentheses
  # if one is found, the expression should be split at that point and the operator and first subexpression stored.
  # a recursive call could handle processing the rest of the string
  # once the first such operator is found, an exception will be thrown if a different operator is found.
  def self.get_subexpressions(expr, operator=nil)
    temp_expr = ""
    open_parens = 0
    temp_operator = ""
    current_complete = false
    letter_found = false
    last_c = ""
    subexpressions = []
    opers = %w[& | - > <]
    raise "Beginning with an operator '#{expr[0]}' invalid in expression '#{expr}'" if opers.include? expr[0]
    raise "Ending with an operator '#{expr[-1]}' invalid in expression '#{expr}'" if opers.include? expr[-1]
    expr.each_char do |c|

      if c == "~"
        raise "Invalid operator '#{temp_operator}' in expression '#{expr}'" if temp_operator.length > 0
        raise "Invalid use of '~' in expression '#{expr}'" if letter?(last_c)
        temp_expr << c
      elsif letter?(c)
        raise "Invalid operator '#{temp_operator}' in expression '#{expr}'" if temp_operator.length > 0
        letter_found = true
        temp_expr << c        
      elsif c == "("
        raise "Invalid use of '(' in expression '#{expr}'" if letter?(last_c) 
        raise "Invalid operator '#{temp_operator}' in expression '#{expr}'" if temp_operator.length > 0
        open_parens += 1
        temp_expr << c
      elsif c == ")"
        raise "Invalid use of ')' in expression '#{expr}'" unless letter?(last_c) || last_c == ')'
        raise "Invalid operator '#{temp_operator}' in expression '#{expr}'" if temp_operator.length > 0
        open_parens -= 1
        temp_expr << c
      elsif c.match(/[&|]/)
        raise "Invalid operator '#{temp_operator << c}' in expression '#{expr}'" if temp_operator.length > 0
        if open_parens == 0
          raise "Operator '#{c}' is not consistent with the current operator '#{operator}' in '#{expr}'" if 
            operator && operator != c
          current_complete = true
          operator = c
        else
          temp_expr << c
        end
      elsif c.match(/\</)
        raise "Invalid operator '#{temp_operator << c}' in expression '#{expr}'" if temp_operator.length > 0
        if open_parens == 0
          temp_operator << c
        else
          temp_expr << c
        end
      elsif c.match(/\-/)
        if open_parens == 0
          temp_operator << c
        else
          temp_expr << c
        end
      elsif c.match(/>/)
        if open_parens == 0
          raise "Invalid operator '#{c}' in expression '#{expr}'" unless temp_operator.length > 0
          temp_operator << c
          raise "Invalid operator '#{temp_operator}' in expression '#{expr}'" unless temp_operator.match(/\-\>/) || temp_operator.match(/\<\-\>/)
          raise "The operator '#{temp_operator}' occurs more than once in '#{expr}'" if operator
          current_complete = true
          operator = temp_operator
          temp_operator = ""
        else
          temp_expr << c
        end
      else
        raise "Invalid character '#{c}' in expression '#{expr}'"
      end
      last_c = c
      break if current_complete
    end
    # temp expr can't consist simply of parens and/or negations
    raise "Invalid expression '#{temp_expr}' in '#{expr}'" unless letter_found
    subexpressions << temp_expr
    if current_complete
      # call recursively with the remainder of the expression
      left_length = temp_expr.length + operator.length

      new_subs, operator = get_subexpressions(expr[left_length..-1], operator)
      subexpressions += new_subs
    end
    return subexpressions, operator
  end

  def expression_equals(expr)
    Formula.remove_outer_parens(self.expression) == Formula.remove_outer_parens(expr)
  end

  def self.wrapped?(expr)
    raise "Parentheses are invalid in the expression '#{expr}'" unless parens_valid?(expr)
    return expr[0] == '(' && expr[-1] == ')' && parens_valid?(expr[1..-2])
  end

  def self.remove_outer_parens(expr)
    raise "Parentheses are invalid in the expression '#{expr}'" unless parens_valid?(expr)
    if wrapped?(expr)
      s = expr.dup[1..-2]
      if wrapped?(s)
        s = remove_outer_parens(s)
        return s
      else
        return s
      end
    else
      return expr
    end
  end

  def self.parens_valid?(expr)
    # step through each character of a string keeping track of open parentheses
    # if it ever goes negative, return false.  Otherwise, return true iff the final count is 0
    open_parens = 0
    expr.each_char do |c|
      case c
      when '('
        open_parens += 1
      when ')'
        open_parens -= 1
      end
      return false if open_parens < 0 
    end
    open_parens == 0
  end


  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``
  # DECOMPOSITION METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``

  # a formula may belong to multiple branches, add one.
  def add_branch(branch)
    @branches << branch
  end

  def decompose(last_line)
    started_at = Time.now
    # note: @decomposed needs to be set at the beginning or we'll end up adding
    # this formula back into the hash in TruthTree when we start adding branches.
    @decomposed = true
    decomp = decomp_type
    if decomp == :positive_and || decomp == :negated_or
      cur_line = decompose_non_branching_ands_and_ors(last_line)
    elsif decomp == :negated_right_arrow
      cur_line = decompose_negated_right_arrow(last_line)
    elsif decomp == :positive_or || decomp == :negated_and
      cur_line = decompose_branching_ands_and_ors(last_line)
    elsif decomp == :positive_right_arrow
      cur_line = decompose_right_arrow(last_line)
    elsif decomp == :positive_double_arrow || decomp == :negated_double_arrow
      cur_line = decompose_double_arrow(last_line)
    end

    puts '%.3f ' % (Time.now - started_at) + "decomposing" if truth_tree.show_timing && !truth_tree.test?
    cur_line
  end

  # NON-BRANCHING DECOMPOSITIONS
  # AND and negated OR decompositions: Add the conjuncts to each branch that this formula belongs to.
  def decompose_non_branching_ands_and_ors(start_line)
    cur_line = start_line
    formulas = @subexpressions.map do |se| 
      cur_line += 1
      Formula.new((@is_negated ? "~" : "") + se, cur_line, self, (@is_negated ? "Negated " : "") + @operator)
    end
    open_branches.each do |b|
      b.add_formulas formulas
    end
    cur_line
  end

  def decompose_negated_right_arrow(start_line)
    cur_line = start_line
    antecedent = @subexpressions[0]
    consequent = @subexpressions[1]
    formulas = []
    cur_line += 1
    formulas << Formula.new(antecedent, cur_line, self, "Negated " + @operator)
    cur_line += 1
    formulas << Formula.new('~' + consequent, cur_line, self, "Negated " + @operator)
    open_branches.each do |b|
      b.add_formulas formulas
    end
    cur_line
  end

  # BRANCHING DECOMPOSITIONS
  # Add a new branch for each operand for each branch that the current formula belongs to (whew!)
  def decompose_branching_ands_and_ors(start_line)
    cur_line = start_line + 1
    formulas = @subexpressions.map do |se|
      Formula.new((@is_negated ? "~" : "") + se, cur_line, self, (@is_negated ? "Negated " : "") + @operator)
    end
    add_formulas_and_branches_for_branching_decomp(formulas, start_line)
    cur_line
  end

  def decompose_right_arrow(start_line)
    cur_line = start_line + 1
    antecedent = @subexpressions[0]
    consequent = @subexpressions[1]
    formulas = []
    formulas << Formula.new("~" + antecedent, cur_line, self, @operator)
    formulas << Formula.new(consequent, cur_line, self, @operator)
    add_formulas_and_branches_for_branching_decomp(formulas, start_line)
    cur_line
  end

  def decompose_double_arrow(start_line)
    cur_line = start_line + 1
    antecedent = @subexpressions[0]
    consequent = @subexpressions[1]
    formulas = []
    formulas << [
      Formula.new((@is_negated ? '~' : '') + antecedent, cur_line, self, (@is_negated ? "Negated " : "") + @operator),
      Formula.new(consequent, cur_line+1, self, (@is_negated ? "Negated " : "") + @operator)]
    formulas << [
      Formula.new((@is_negated ? '' : '~') + antecedent, cur_line, self, (@is_negated ? "Negated " : "") + @operator),
      Formula.new('~' + consequent, cur_line+1, self, (@is_negated ? "Negated " : "") + @operator)]
      cur_line += 1
    add_formulas_and_branches_for_branching_decomp(formulas, start_line)
    cur_line
  end

  # Given an array of formulas (which could be nested one level), and an array of branches, 
  def add_formulas_and_branches_for_branching_decomp(formulas, start_line)
    # for each of the original formula's ORIGINAL open branches
    brs = open_branches
    branch_groups = []
    brs.each do |b|
      branch_groups << create_branches_for_formulas(b, formulas, start_line)
    end

    # Get the 'branch group' for each branch the original formula was in.
    # for each branch group, we must retain at least one member when removing duplicates.
    # Furthermore, we can't remove a branch for duplication unless we have more than one such branch
    branch_groups.each do |bg|
      obs = bg.select { |b| !b.closed? && b.dup_closing_line && b.dup_closing_line > start_line }
      obs.each do |sb|
        break if bg.select { |b| !b.closed? && b.dup_closing_line && b.dup_closing_line > start_line }.count <= 1
        sb.close_due_to_duplicates
      end
    end
  end

  def create_branches_for_formulas(branch, formulas, start_line)
    # for each of the remaining formulas (or arrays of formulas), 
    # create a new branch and add the formula (or array of formulas) to it.
    new_branches = []
    formulas[1..-1].each do |f|

      bd = truth_tree.add_branch branch
      # add the existing formulas.  This will take care of updating the atoms hash for the new branch
      bd.add_formulas branch.formulas
      if f.kind_of?(Array)
        bd.add_formulas f
      else
        bd.add_formulas [f]
      end
      new_branches << bd
    end
    # now add the first formula (or array of formulas) to the current branch
    # do this last, so the newly-created branches are not affected...
    if formulas[0].kind_of? Array
      branch.add_formulas formulas[0]
    else
      branch.add_formulas [formulas[0]]
    end
    # Don't worry about the output for test branches.  It will break because line numbers aren't correct.
    truth_tree.append_branch_orders(branch, new_branches, start_line+1) unless truth_tree.test?
    new_branches << branch
  end

  # decomposition method for this formula
  def decomp_type
    self.class.decomp_type(@is_negated, @operator)
  end

  def self.decomp_type(is_negated, operator)
    if is_negated
      case operator
      when "&"
        :negated_and
      when "|"
        :negated_or
      when "->"
        :negated_right_arrow
      when "<->"
        :negated_double_arrow
      else
        raise "Unknown operator #{@operator}"
      end
    else
      case operator
      when "&"
        :positive_and
      when "|"
        :positive_or
      when "->"
        :positive_right_arrow
      when "<->"
        :positive_double_arrow
      else
        raise "Unknown operator #{@operator}"
      end
    end
  end

end


