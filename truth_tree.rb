# A truth tree stores its data as an array of branches, each of which is open or closed.
# It is complete when
#   either a completed open branch is found 
#   or when all branches have been fully decomposed and no open branches are found
# A truth tree chooses the next decomposition step according to some rules
#   perform non-branching decompositions before branching ones.
#   perform complex decompositions before simpler ones.
# A truth tree asks its branches whether they are complete at each step. (A branch
#   is complete if either it is closed or if it is open and fully-decomposed)
require 'ruby-debug'
class TruthTree
  
  attr_reader :branches, :expressions, :show_timing, :total_atoms, :initial_formulas, :non_decomposed_formulas,
              :fully_initialized, :close_due_to_duplicates, :branch_orders, :stop_at_atom_count

  attr_accessor :last_line # we need to be able to update this for the test tree.

  def initialize(expressions, show_timing=false, show_true_atoms_each_step=false, grandchildren=true, pick_first=true,
    stop_at_atom_count=false, expected_atom_count=nil, close_due_to_duplicates=true, is_test=false)
    @expressions = expressions
    @pick_first = pick_first
    @grandchildren = grandchildren
    @show_true_atoms_each_step = show_true_atoms_each_step
    @show_timing = show_timing
    @stop_at_atom_count = stop_at_atom_count # helpful with certain classes of problems
    @close_due_to_duplicates = close_due_to_duplicates
    @branches = []
    add_branch nil
    @last_line = nil
    @is_test = is_test
    @branch_orders = { 1 => [0] }
    @expression_arr = get_initial_expressions
    @total_atoms = get_total_atom_count
    puts "Counted #{@total_atoms} atoms." if @show_timing || @show_true_atoms_each_step
    raise "Atom count did not match the expected value." if expected_atom_count && expected_atom_count != @total_atoms
    @initial_formulas = get_initial_formulas
  end

  def self.get_test_tree
    test_tree = TruthTree.new("", false, false, @grandchildren, @pick_first, 
      @stop_at_atom_count, nil, @close_due_to_duplicates, true)
    test_tree.clear_branches
    test_tree.set_fully_initialized
  end

  # This must be called outside of the constructor due to tree/branch coupling
  def add_initial_formulas
    branches[0].add_formulas @initial_formulas
    @fully_initialized = true
  end

  def set_fully_initialized
    @fully_initialized = true
    self
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # INTROSPECTION METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # the tree is complete if all formulas have been decomposed or if all its branches are complete.  
  # Note that it may not be necessary to process a tree to completion.  
  # We can stop if an open, complete branch has been found.
  def complete?
    return true if non_decomposed_formulas.count == 0
    @branches.inject(true) { |all, b| all && b.complete? }
  end

  def test?
    @is_test
  end

  def model_branches
    @branches.select { |b| b.open_complete? }
  end

  def open_non_decomposed_branches
    @branches.select { |b| !b.closed? && !b.decomposed? }
  end

  def non_decomposed_formulas
    formulas = []
    open_non_decomposed_branches.each { |b| formulas |= b.non_decomposed_formulas } 
    formulas
  end

  def open_branches
    @branches.select { |b| !b.closed? }
  end

  # Common atoms are simply atoms whose values are the same across all open branches
  # We can think of them as a partial solution to a large problem.
  def common_atoms
    common = {}
    b0 = open_branches[0]
    b0.atoms.each do |letter, value|
      add_it = true
      open_branches.each do |b| 
        if b.atoms[letter] != value
          add_it = false
          break
        end
      end
      common[letter] = value if add_it
    end
    common
  end

  def common_atoms_array
    common_atoms.map { |letter, negated| (negated ? "~" : "") + "#{letter}" }
  end

  def common_atoms_text
    common = common_atoms
    Branch.model_text(common)
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # INITIAL PROCESSING METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def get_total_atom_count
    atoms = []
    @expression_arr.each do |exp|
      is_negated, operator, expr, subexpressions = Formula.parse(exp)
      atoms |= get_atoms_for_expression(exp)
    end
    atoms.count
  end

  def get_atoms_for_expression(expression)
    atoms = []
    is_negated, operator, expr, subexpressions = Formula.parse(expression)
    as = subexpressions.select { |s| Formula.is_atomic(s) }
    atoms |= as.map do |atom|
      letter, negated = Formula.atom_attributes(atom)
      letter
    end
    non_atoms = subexpressions.select { |s| !Formula.is_atomic(s) }
    non_atoms.each { |s| atoms |= get_atoms_for_expression(s) }
    atoms
  end

  def get_initial_expressions
    clean_text = self.class.clean(@expressions).strip
    clean_text.split("\n").select { |exp| exp.length > 0 && !exp.match(/\A\#/) }
  end

  def get_initial_formulas
    formulas = @expression_arr.each_with_index.map { |expr, i| Formula.new(expr, i+1) }
    @last_line = formulas.count
    formulas
  end

  # Remove whitespace from an expression and perform some replacements.
  def self.clean(expression)
    expr = expression.gsub(/[ \r\t]/, '')
    expr.gsub!(/[\[\{]/, '(')   #standard parens
    expr.gsub!(/[\]\}]/, ')')
    expr.gsub!(/\!/, '~')       # standard negation
    expr.gsub!(/\^/, '&')       # standard conjuncion
    expr
  end


  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # FORMULA SELECTION
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def select_next_formula

    selection_rule = nil
    idx = nil
    formula = nil
    add_initial_formulas unless @fully_initialized

    ndfs = non_decomposed_formulas
    ndf_count = ndfs.count
    started_at = Time.now

    puts "Line: #{last_line}, Open Brs: #{open_branches.count}" if @show_timing
    unless formula
      idx = ndfs.index { |f| !f.branching? }
      if idx
        formula = ndfs[idx]
        selection_rule = :nonbranching 
        @formula_selection = "With Non-branching expression. Expression: #{formula.expression}"
      end
      puts '%.3f ' % (Time.now - started_at) + (idx ? "found nonbranching at #{idx} of #{ndf_count} non-decomposed. Line #{last_line} Brs: #{open_branches.count}" : "fruitless search for nonbranching") if @show_timing
    end
    started_at = Time.now

    unless formula
      idx = ndfs.index { |f| f.has_closing_subexpressions? }

      if idx
        formula = ndfs[idx]
        selection_rule = :closing_subexpressions
        @formula_selection = "With Closing Subexpressions. Expression: #{formula.negated? ? '~' : '' }#{formula.expression}"
      end
      puts '%.3f ' % (Time.now - started_at) + (idx ? "found with closing subs at #{idx} of #{ndf_count} non-decomposed" : "fruitless search for closing subs") if @show_timing
    end
    started_at = Time.now

    unless formula || !@close_due_to_duplicates
      idx = ndfs.index { |f| f.has_closing_subexpressions_dupes? }
      if idx
        formula = ndfs[idx]
        selection_rule = :closing_subexpressions_dupes
        @formula_selection = "With Closing Subexpressions considering duplicates. Expression: #{formula.negated? ? '~' : '' }#{formula.expression}"
      end
      puts '%.3f ' % (Time.now - started_at) + (idx ? "found with closing subs dupes at #{idx} of #{ndf_count} non-decomposed" : "fruitless search for closing subs dupes") if @show_timing
    end
    started_at = Time.now

    unless formula || !@grandchildren
      idx = ndfs.index { |f| f.test_tree_has_closing_subexpressions? }
      if idx
        formula = ndfs[idx]
        selection_rule = :closing_sub_subexpressions
        @formula_selection = "With Closing Sub-subexpressions. Expression: #{formula.negated? ? '~' : '' }#{formula.expression}"
      end
      puts '%.3f ' % (Time.now - started_at) + (idx ? "found with closing sub-subs at #{idx} of #{ndf_count} non-decomposed" : "fruitless search for closing sub-subs") if @show_timing
    end
    started_at = Time.now

    unless formula || !@pick_first
      formula = ndfs[0]
      selection_rule = :picked_first
      @formula_selection = "Couldn't find a nice one to process!  Expression: #{formula.expression}"
      puts '%.3f ' % (Time.now - started_at) + "picked first of #{ndf_count} non-decomposed" if @show_timing
    end

    return formula, selection_rule
  end

  def clear_branches
    @branches = []
    self
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # TREE PROCESSING
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def add_branch(parent_branch)
    branch = Branch.new self, parent_branch
    @branches << branch
    branch
  end

  def process_all(display_results=true, summary_only=false)
    started_at = Time.now
    add_initial_formulas unless @fully_initialized
    begin
      process_next(false) until (@force_stop || complete? )
    rescue SystemExit, Interrupt
      @force_stop = true
    end
    display_tree(summary_only) if display_results 
    puts "Run time: #{Time.now - started_at}" if display_results
  end

  def process_through_line(line, display_results=true, summary_only=false)
    started_at = Time.now
    add_initial_formulas unless @fully_initialized
    process_next(false) until (@force_stop || last_line == line || complete?)
    display_tree(summary_only) if display_results 
    puts "Run time: #{Time.now - started_at}" if display_results
  end

  def process_next(display_results=true, summary_only=true)
    return nil if @fully_initialized && complete?
    add_initial_formulas unless @fully_initialized
    formula, selection_rule = select_next_formula
    unless formula
      @force_stop = true 
    else
      begin 
        @last_line = formula.decompose @last_line
      rescue SystemExit, Interrupt
        # delete any branches created in the current step to leave the tree in a healthy state.
        branches.reject! { |b| b.created_in_line > @last_line }
        @force_stop = true
      end
      puts open_branches[0].true_atoms_text if @show_true_atoms_each_step && open_branches.count == 1
    end
    display_tree(summary_only) if display_results 
  end

  def process_line(line, display_results=true, summary_only=true)
    raise "Nothing to process.  Tree has been fully processed" if @fully_initialized && complete?
    add_initial_formulas unless @fully_initialized
    ndfs = non_decomposed_formulas
    formula = ndfs.detect { |f| f.line == line }
    raise "No formula could be found for line number #{line}" unless formula
    raise "Line number #{line} has already been decomposed" if formula.decomposed?
    raise "Line number #{line} is atomic" if formula.atomic?
    @last_line = formula.decompose(@last_line)
    display_tree(summary_only) if display_results 
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # DISPLAY METHODS
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def status
    open_branch_ct = open_branches.count
    non_decomposed_count = non_decomposed_formulas.count
    open_nondecomp_ct = open_non_decomposed_branches.count
    branch_model_text = model_branches.map { |b| "Branch #{@branches.index(b)+1}:  #{b.model_text}" }.join("\n")
    if open_branch_ct == 1 && non_decomposed_count > 0
      b = open_branches[0]
      partial_solution_text = "Branch #{@branches.index(b)+1}:  #{b.model_text true}\n"
    else
      partial_solution_text = ""
    end
    model_info = model_branches.count > 0 ? 
      (open_nondecomp_ct > 0 ? "Possible Model(s) were found:" : "Model(s) were found:") + "\n#{branch_model_text}" : 
      "No model could be found."
    message = complete? ? "Tree is complete.\n#{model_info}\n" : 
      "Tree is not yet completely processed.\n" +
      "There are #{open_branch_ct} open branches and #{@non_decomposed_count} non-decomposed formulas.\n" +
      "Partial Solution: \n#{partial_solution_text}"
    message << "\nThe last step taken was #{@formula_selection} in line #{@last_line}." if @show_timing
    message
  end

  def get_branches_and_formulas(line)
    # Each 'line' is really 3 lines.  The first identifies the branches by number (index within tree).  
    # The second lists the expressions separated by spaces
    # The third prints x's for closed branches.
    return nil if line > last_line
    results = []
    brs = branches.select { |b| b.created_in_line <= line && (!b.closed? || b.closing_line >= line ) }
    brs = brs.sort_by { |b| b.index_in_branch_orders(line) }
    brs.each do |b|
      # there should only be one formula for a given branch and line, right?
      results << [ b, b.formulas.detect { |f| f.line == line } ]
    end
    return results
  end

  def display_line_info(line)
    line_text = "Line #{line}.  "
    lines = [line_text, " "*line_text.length, " "*line_text.length]
    info = get_branches_and_formulas line
    formulas = info.detect { |b, f| f }
    return unless formulas
    # there should be at least one formula on the line. Any one will do.
    first_formula = formulas[1]
    parent_formula = first_formula.parent_formula
    decomp_text = parent_formula ? "#{parent_formula.line}. #{first_formula.decomp_method}" : "Set Member"

    return "There is no line #{last_line} in this tree." unless info
    info.each do |b, f|
      branch_text = "Br. #{branches.index(b) + 1}"
      branch_closing_info = "x" + (b.duplicates_line ? ": D#{b.duplicates_line}" : "")
      formula_text = f ? f.expression : '|'
      col_width = [branch_text.length, formula_text.length].max + 2
      lines[0] << branch_text.center(col_width)
      lines[1] << formula_text.center(col_width)
      lines[2] << (b.closing_line == line ? branch_closing_info.center(col_width) : "".center(col_width))
    end
    lines[0] << "    " + decomp_text
    lines.join("\n")
  end

  def display_tree(summary_only=false)
    unless summary_only
      (1..last_line).each do |l|
        puts display_line_info l
        puts
      end
    end
    puts
    puts status
  end

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # MANAGE THE ORDERING OF BRANCHES FOR DISPLAY
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def append_branch_orders(parent_branch, child_branches, line)
    # given the index of a parent branch and an array of child indices with respect to all branches,
    # get the last branch_orders subarray and insert the subarray after the index of the parent_index
    parent_idx = branches.index(parent_branch)
    child_indices = child_branches.map { |cb| branches.index(cb) }
    ar = @branch_orders[line] # in this case, we're adding more branches for the same line
    ar = @branch_orders.values.last.dup unless ar
    unless ar.index(parent_idx)
      debugger
    end
    ar = ar.insert(ar.index(parent_idx)+1, *child_indices)
    @branch_orders[line] = ar
  end

  def get_branch_orders_for_line(line)
    cur_line = line
    cur_line -= 1 until @branch_orders[cur_line]
    @branch_orders[cur_line]
  end



end
