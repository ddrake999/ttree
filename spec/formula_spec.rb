require 'spec_helper'

describe Formula do 
  before :each do
    @formula = Formula.new("~((p&q)|(q->r))", 1)
  end

  describe "#new" do
    before { @formula = Formula.new("~((p&q)|(q->r))", 1) }
    it "takes one or three parameters and returns a Formula object" do
      @formula.should be_an_instance_of Formula
    end
  end

  describe "#set_member?" do
    describe "when the formula has no parent formula" do
      before { @formula = Formula.new("~((p&q)|(q->r))", 1) }
      it "should be true" do
        @formula.set_member?.should eql true
      end
    end
  end

  describe "#expression" do
    before { @formula = Formula.new("~((p&q)|(q->r))", 1) }
    it "should be the string we passed to the constructor" do
      @formula.expression.should eql "~((p&q)|(q->r))"
    end
  end

  describe "#atomic?" do
    describe "when the formula is atomic" do
      it "should be true" do
        Formula.new("~p", 1).atomic?.should eql true
        Formula.new("q", 1).atomic?.should eql true
        Formula.new("abc", 1).atomic?.should eql true
        Formula.new("~abc", 1).atomic?.should eql true
        Formula.new("~~abc", 1).atomic?.should eql true
      end
    end
    describe "when the formula is not atomic" do
      it "should be false" do
        Formula.new("~p&q", 1).atomic?.should eql false
        Formula.new("(q&p)|r", 1).atomic?.should eql false
      end
    end
  end

  describe "#parens_valid?" do
    describe "with an invalid expression" do
      it "should be false" do
        Formula.parens_valid?("a)|(b").should eql false
      end
    end
    describe "with a valid expression" do
      it "should be true" do
        Formula.parens_valid?("(a)|(b)").should eql true
      end
    end
  end

  describe "#remove_outer_parens" do
    describe "with an expression that does not contain any internal parentheses" do
      it "should strip off all outer parens" do
        Formula.remove_outer_parens("((a|b))").should eql "a|b"
      end
    end
    describe "with an expression that does contains only internal parentheses" do
      it "should not change the string" do
        Formula.remove_outer_parens("(a)|(b)").should eql "(a)|(b)"
      end
    end
    describe "with an expression that contains a mix of internal and external parentheses" do
      it "should only remove the outer ones" do
        Formula.remove_outer_parens("(((a&(b|c))|(b)))").should eql "(a&(b|c))|(b)"
      end
    end
  end

  describe "#wrapped?" do
    describe "with an expression that does not contain any internal parentheses" do
      it "should be true" do
        Formula.wrapped?("((a|b))").should eql true
      end
    end
    describe "with an expression that does contains only internal parentheses" do
      it "should be false" do
        Formula.wrapped?("(a)|(b)").should eql false
      end
    end
    describe "with an expression that doesn't begin with parentheses" do
      it "should be false" do
        Formula.wrapped?("a|b").should eql false
      end
    end
    describe "with an expression that contains a mix of internal and external parentheses" do
      it "should be true" do
        Formula.wrapped?("(((a&(b|c))|(b)))").should eql true
      end
    end
  end

  describe "#handle_negation" do
    describe "with an expression that has a single negation" do
      it "should be true" do
        Formula.handle_negation("~(a|b)").should eql ["a|b", true]
        Formula.handle_negation("(~(a|b))").should eql ["a|b", true]
      end
    end
    describe "with an expression that has no initial negation" do
      it "should be true" do
        Formula.handle_negation("(~a|b)").should eql ["~a|b", false]
        Formula.handle_negation("((~a|b))").should eql ["~a|b", false]
        Formula.handle_negation("55_jos|55_kai|55_mac|55_mad").should eql ["55_jos|55_kai|55_mac|55_mad", false]
      end
    end
    describe "with an expression that has multiple initial negations" do
      it "should be true" do
        Formula.handle_negation("~~q").should eql ["q", false]
        Formula.handle_negation("~~~q").should eql ["~q", false]
        Formula.handle_negation("~~~~q").should eql ["q", false]
        Formula.handle_negation("~~(a|b)").should eql ["a|b", false]
        Formula.handle_negation("~~~(a|b)").should eql ["a|b", true]
        Formula.handle_negation("~(~(~a|b))").should eql ["~a|b", false]
        Formula.handle_negation("~(~(~a|b))").should eql ["~a|b", false]

      end
    end
  end

  describe "#get_subexpressions" do

    describe "when the subexpression is a single expression" do
      it "should return the expression" do
        Formula.get_subexpressions("(a->b)","&").should eql [["(a->b)"],"&"]
        Formula.get_subexpressions("(abc->b)","&").should eql [["(abc->b)"],"&"]
      end
    end
    describe "when the operator is inconsistent" do
      it "should raise an error" do
        expect { Formula.get_subexpressions("a|b&c") }.to raise_error, 
          "Operator '&' is not consistent with the current operator '|' in 'a|b&c'"
      end
    end

    describe "when the operator is consistent" do
      describe "when an expression begins with an operator" do
        it "should raise an error" do
          expect { Formula.get_subexpressions("&b") }.to raise_error "Beginning with an operator '&' invalid in expression '&b'"
        end
      end
      describe "when an expression ends with an operator" do
        it "should raise an error" do
          expect { Formula.get_subexpressions("b&") }.to raise_error "Ending with an operator '&' invalid in expression 'b&'"
        end
      end
      describe "when an unexpected character is found" do
        it "should raise an error" do
          expect { Formula.get_subexpressions("a<-$b") }.to raise_error "Invalid character '$' in expression 'a<-$b'"
        end
      end
      describe "when the operator is invalid" do
        it "should raise an error" do
          expect { Formula.get_subexpressions("a<b") }.to raise_error "Invalid operator '<' in expression 'a<b'"
          expect { Formula.get_subexpressions("a-b") }.to raise_error "Invalid operator '-' in expression 'a-b'"
          expect { Formula.get_subexpressions("a<<-b") }.to raise_error "Invalid operator '<<' in expression 'a<<-b'"
        end
      end
      describe "when the operator is used more than once" do
        describe "when that is not allowed for the operator" do
          it "should raise an error" do
            expect { Formula.get_subexpressions("a->b->c") }.to raise_error,
              "The operator '->' occurs more than once in 'a->b->c'"
          end
        end
        describe "when that is allowed for the operator" do
          it "should return correct values" do
            Formula.get_subexpressions("a&b&c").should eql [["a","b","c"], "&"]
            Formula.get_subexpressions("azy&bdf&cmn").should eql [["azy","bdf","cmn"], "&"]
            Formula.get_subexpressions("~(a|b)&~b&(c&d)").should eql [["~(a|b)","~b","(c&d)"], "&"]
            Formula.get_subexpressions("~(azy|b)&~bdf&(c&d)").should eql [["~(azy|b)","~bdf","(c&d)"], "&"]
          end
        end
      end
      describe "when the operator is used only once" do
        it "should return correct values" do
          Formula.get_subexpressions("a&b").should eql [["a","b"], "&"]
          Formula.get_subexpressions("azy&bdf").should eql [["azy","bdf"], "&"]
          Formula.get_subexpressions("a->b").should eql [["a","b"], "->"]
          Formula.get_subexpressions("azy->bdf").should eql [["azy","bdf"], "->"]
          Formula.get_subexpressions("(a|b)&(c|b)").should eql [["(a|b)","(c|b)"], "&"]
          Formula.get_subexpressions("(a|b)->(c|b)").should eql [["(a|b)","(c|b)"], "->"]
        end
      end
    end
  end

  describe "#Formula.parse" do
    describe "with valid expressions" do
      it "should parse an expression into subexpressions" do
        Formula.parse("55_jos|55_kai|55_mac|55_mad").should eql [false,'|',
          "(55_jos|55_kai|55_mac|55_mad)", ["55_jos", "55_kai", "55_mac", "55_mad"]]
      end
      it "should parse an expression into subexpressions" do
        Formula.parse("a&b").should eql [false,'&',"(a&b)", ["a", "b"]]
      end
    end
    describe "with some bad expressions" do
      it "should give helpful error messages" do
        expect { Formula.parse("p&q~r")}.to raise_error "Invalid use of '~' in expression 'q~r'" 
      end
      it "should give helpful error messages" do
        expect { Formula.parse("a&~&b")}.to raise_error "Invalid expression '~' in '~&b'" 
      end
      it "should give helpful error messages" do
        expect { Formula.parse("a(~)")}.to raise_error "Invalid use of '(' in expression 'a(~)'"
      end
      it "should give helpful error messages" do
        expect { Formula.parse("a&(~b|)c")}.to raise_error "Invalid use of ')' in expression '(~b|)c'"
      end
      it "should give helpful error messages" do
        expect { Formula.parse("a&(~b|)c")}.to raise_error "Invalid use of ')' in expression '(~b|)c'"
      end

    end

  end

end

