# A truth tree stores its data as an array of branches, each of which is open or closed.
# It is complete when
#   either a completed open branch is found 
#   or when all branches have been fully decomposed and no open branches are found
# A truth tree chooses the next decomposition step according to some rules
#   perform non-branching decompositions before branching ones.
#   perform complex decompositions before simpler ones.
# A truth tree asks its branches whether they are complete at each step. (A branch
#   is complete if either it is closed or if it is open and fully-decomposed)

require 'spec_helper'

describe TruthTree do 

  describe "#new" do
    describe "with a bad expression" do
      it "should give a helpful error message" do
        expect { TruthTree.new("p\np&q~r") }.to raise_error "Invalid use of '~' in expression 'q~r'"
      end
    end

    before do
      @tree = TruthTree.new("r&~s\n~((p&q)|(q->r))") 
      @tree.add_initial_formulas
    end
    it "takes a string with newline-separated expressions and returns a Truth tree object" do
      @tree.should be_an_instance_of TruthTree
    end
  end

  describe "#clean" do
    it "should remove spaces and substitute standard parentheses for square and curly braces" do
      TruthTree.clean("[~{ a & ( b|c )} & (a&b)] \r\n a & \t c ").should eql "(~(a&(b|c))&(a&b))\na&c"
    end
  end

  describe "#complete?" do
    describe "when the truth tree hasn't been generated yet" do
      before do
        @tree = TruthTree.new("r&~s\n~((p&q)|(q->r))") 
        @tree.add_initial_formulas
      end
      it "should be false" do
        @tree.complete?.should eql false
      end
    end
  end

  describe "#get_total_atom_count" do
    before do
      @tree = TruthTree.new("r&~s\n~((p&q)|(q->r))") 
      @tree.add_initial_formulas
    end
    it "should be true" do
      @tree.get_total_atom_count.should eql 4
    end
  end

  describe "#branches" do
    describe "when the truth tree hasn't been generated yet" do
      before do
        @tree = TruthTree.new("r&~s\n~((p&q)|(q->r))") 
        @tree.add_initial_formulas
      end
      it "should have one branch" do
        @tree.branches.count.should eql 1
      end
      its "branch should contain two formulas" do
        @tree.branches[0].formulas.count.should eql 2
      end
      its "formulas should reference back to the branch" do
        @tree.branches[0].formulas[0].branches[0].should eql @tree.branches[0]
      end
    end
  end

  describe "#process_all" do
    describe "when branching is minimized by considering grandchildren" do
      before do
        @tree = TruthTree.new "p\nq\nr\ns\nt|v\nw|x\n(~p&~q)|(~p&~q)"
        @tree.process_all false
      end
      it "should only have two branches" do
        @tree.branches.count.should eql 2
      end
    end
    describe "when only one branch has duplicates" do
      before do
        @tree = TruthTree.new "p \n q|p \n (r&~q)|(r&~q)"
        @tree.process_all false
      end
      it "should not allow the p branch to be eliminated" do
        @tree.model_branches.count.should eql 2
      end
    end

    describe "when two branches have duplicates" do
      before do
        @tree = TruthTree.new "p \n q \n r|s \n q|p"
        @tree.process_all false
      end
      it "should allow one branch to be eliminated" do
        @tree.model_branches.count.should eql 2
      end
    end

    describe "Judy says 'Punch and I are knaves'" do
      before do 
        @tree = TruthTree.new("j <-> (~j & ~p)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 1
        @tree.model_branches[0].model_text.should eql "j = false, p = true\nThe following 1 atoms are true:\np\n"
      end
    end
    describe "Judy says 'Punch and I are knaves' with multichar letters" do
      before do 
        @tree = TruthTree.new("judy <-> (~judy & ~punch)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 1
        @tree.model_branches[0].model_text.should eql "judy = false, punch = true\nThe following 1 atoms are true:\npunch\n"
      end
    end
    describe "Show that (p -> ~p) is not a contradiction" do
      before do
        @tree = TruthTree.new("(p -> ~p)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 2
        @tree.model_branches[0].model_text.should eql "p = false\nThe following 0 atoms are true:\n\n"
      end
    end
    describe "Show that (p -> ~p) is not logically true (so from prev. example, contingent)" do
      before do
        @tree = TruthTree.new("~(p -> ~p)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 1
        @tree.model_branches[0].model_text.should eql "p = true\nThe following 1 atoms are true:\np\n"
      end
    end
    describe "Holmes: Serving God or Mammon -> Can't serve God" do
      before do
        @tree = TruthTree.new "~(g&m) \n s|m \n ~g|l \n ~l|~s \n g"
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Holmes: Serving God or Mammon -> Can't serve God with multichar letters" do
      before do
        @tree = TruthTree.new "~(god&mammon) \n starve|mammon \n ~god|live \n ~live|~starve \n god"
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Class example 1: Show (p&s)->r, r|t, t->(q&p), ~q|u doesn't imply p->(s|u)" do
      before do
        @tree = TruthTree.new("(p&s)->r \n r|t \n t->(q&p) \n ~q|u \n ~(p->(s|u))")
        @tree.process_all false
      end
      it "should have a model" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 2
        @tree.model_branches[0].model_text.should eql "p = true, q = false, r = true, s = false, t = false, u = false\nThe following 2 atoms are true:\np, r\n"
      end
    end
    # standard argument forms
    describe "Modus Ponens" do
      before do
        @tree = TruthTree.new("(p->q)&p \n ~q")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Modus Tollens" do
      before do
        @tree = TruthTree.new("(p->q)&~q \n p")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Hypothetical Syllogism" do
      before do
        @tree = TruthTree.new("(p->q)&(q->r) \n ~(p->r)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Disjunctive Syllogism" do
      before do
        @tree = TruthTree.new("(p|q)&~p \n ~q")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Constructive Dilemma" do
      before do
        @tree = TruthTree.new("((p->q)&(r->s)&(p|r)) \n ~(q|s)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Destructive Dilemma" do
      before do
        @tree = TruthTree.new("((p->q)&(r->s)&(~q|~s)) \n ~(~p|~r)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Bidirectional Dilemma" do
      before do
        @tree = TruthTree.new("((p->q)&(r->s)&(p|~s)) \n ~(q|~r)")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Simplification" do
      before do
        @tree = TruthTree.new("(p&q) \n ~p")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Conjunction" do
      before do
        @tree = TruthTree.new("p \n q \n ~(p|q) ")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end
    describe "Addition" do
      before do
        @tree = TruthTree.new("p \n ~(p|q) ")
        @tree.process_all false
      end
      it "should be complete" do
        @tree.complete?.should eql true
        @tree.model_branches.count.should eql 0
      end
    end

  end

  describe "#process_next" do
    describe "with a single processing step" do
      before do
        @tree = TruthTree.new "(p&q)->(~p|q)"
        @tree.process_next false
      end
      it "should have two branches" do
        @tree.branches.count.should eql 2
      end
      its "first branch should have a correct new formula" do
        @tree.branches[0].formulas[1].expression.should eql "~(p&q)"
      end
      its "second branch should have a correct new formula" do
        @tree.branches[1].formulas[1].expression.should eql "(~p|q)"
      end
    end
    describe "with three processing steps" do
      before do
        @tree = TruthTree.new "(p&q)->(~p|q)" 
        # ~(p&q)      (~p|q)
        # ~p    ~q    
        # ~p  q  ~p  q

        @tree.process_next false
        @tree.process_next false
        @tree.process_next false
      end
      it "should have four branches" do
        @tree.branches.count.should eql 4
      end
      # todo: clarify the order of branching!!!

      # its "first branch should have a correct new formula" do
      #   @tree.branches[0].formulas[2].expression.should eql "~p"
      # end
      # its "second branch should have a correct new formula" do
      #   @tree.branches[1].formulas[2].expression.should eql "q"
      # end
      # its "third branch should have a correct new formula" do
      #   @tree.branches[2].formulas[2].expression.should eql "(~p|q)"
      # end
    end
  end


end

