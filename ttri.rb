#!/usr/bin/env ruby
require_relative './truth_tree'
require_relative './branch'
require_relative './formula'
require 'readline'

def show(list)
  list.each.with_index { |line, i| puts "#{i+1}. #{line}"}
end

def go_again?
  puts
  puts "Generate another tree? y/n"
  resp = gets.chomp.downcase
  until ['y','n'].include? resp
    puts "Please enter y or n"
    resp = gets.chomp.downcase
  end
  resp == 'y'
end

MSG = <<EOT
Welcome to the interactive truth tree processor!
Please enter a set of expressions to evaluate, one per line.

Example: [(p&q)->(q&r&~s)]<->[(s|~q)&(~p|r)]

After entering your statements, 
press <Enter> on a blank line to start processing.
EOT

puts MSG
go_again = true
while go_again
  puts "Enter your your expression set:"
  list = []
  while line = Readline.readline('> ', true) and line.length > 0
    list << line
    show list
  end
  begin 
    tree = TruthTree.new list.join("\n")
    tree.process_all
  rescue => e
    puts e.message
  end
  go_again = go_again?
end
puts "Bye!"

