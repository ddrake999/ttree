#!/usr/bin/env ruby
require_relative './truth_tree'
require_relative './branch'
require_relative './formula'

# Generate a truth tree from rules in the specified file.
# Options are 
cur_path = File.expand_path File.dirname(__FILE__)

def get_option(regex)
  idx = ARGV.find_index { |e| e.match( regex ) }
  if idx 
    mat = ARGV[idx].match( regex)
    ARGV.delete_at(idx)
    value = mat[1] ? mat[1] : true
  else
    value = nil
  end
end

help_text = <<EOT
Please specify a file containing statements to process
Example usage: 
For a smaller problem:
./ttree.rb samples/example1.txt > output.txt
For a larger problem:
./ttree.rb -p -f -g samples/generated1.txt > output.txt
Options:
-d : Suppress tree display
-s : Suppress all display (tree and summary)
-t : Display timing of internals of slow processes
-u : Display true atoms at each step of process_next
-f : Don't allow 'picking the first' as a formula selection method. 
     It tends to produce lots of branches resulting in very long run times 
     for large problems.  If this is specified, the tree will never have more 
     than 1 open branch.
-g : Don't check for closing grandchildren expressions.
     The algorithm for this can perform badly for large problems.
-p : Stop processing when the atom count is reached.
     This may not result in a true model, but is useful if we know a priori that
     a model exists and we've specified -f so the tree only has 1 open branch.
-a : Expected atom count. If specified, an error will be raised 
     if this value doesn't match the calculated count
-u : Do not close branches due to duplicates. The default is to close 
     a newly created branch if the last expression is already in the branch 
     and at least one open branch will remain for the original formula.
     It may be useful to specify this option for performance reasons.
EOT
if ARGV.count == 0
  puts help_text
else
  suppress_tree_display = get_option(/-d/)
  suppress_all_display = get_option(/-s/)
  timing = get_option(/-t/)
  true_atoms = get_option(/-u/)
  exclude_pick_first = get_option(/-f/)
  exclude_grandchildren = get_option(/-g/)
  stop_at_atom_count = get_option(/-p/)
  expected_atom_count = get_option(/-a/)
  bypass_close_due_to_duplicates = get_option(/-u/)

  if test = ARGV[0].match( /-./)
    puts "Unknown option #{test}"
    puts
    puts help_text
    exit
  end

  filename = ARGV[0]
  filepath = File.expand_path File.join(cur_path, filename) 
  contents = File.open(filepath, 'r') { |f| f.read } 
  puts "Processing '#{filename}' with contents:\n\n#{contents}"
  puts
  puts "----------------------------------------"
  puts
  begin
    tree = TruthTree.new contents, timing, true_atoms, !exclude_grandchildren, !exclude_pick_first, stop_at_atom_count, 
      expected_atom_count, !bypass_close_due_to_duplicates
    tree.process_all !suppress_all_display, suppress_tree_display
  rescue => e
    puts e.message
    puts e.backtrace
  end
  puts "----------------------------------------"
  puts
end

