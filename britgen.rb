#!/usr/bin/env ruby
require_relative './truth_tree'
require_relative './branch'
require_relative './formula'
require 'pp'

# A Utility for generating a sample set of rules, such as in samples/generated1.txt

cur_path = File.expand_path File.dirname(__FILE__)
if ARGV.count != 1
  puts "Please specify a definition file to process"
else
  filename = ARGV[0]
  filepath = File.expand_path File.join(cur_path, filename) 
  contents = File.open(filepath, 'r') { |f| f.read } 
  lines = contents.strip.split("\n")
  cats = []
  lines.each do |l|
    vals = l.split
    cats << vals
  end

  all_stmts = []
  for row in 0...cats.count-1 
    cat = cats[row]                       # [All Bre Kas Mac Raq]
    for next_row in row+1...cats.count
      next_cat = cats[next_row]             # [fig kit lic pos sno]
      big_arr = []
      for col in 0...cat.count
        tmp_arr = []
        for next_col in 0...next_cat.count
          stmt = cat[col] + "_" + next_cat[next_col] 
          tmp_arr << stmt                   # [All_fig All_kit All_lic All_pos All_sno]
        end
        big_arr << tmp_arr
      end
      all_stmts << big_arr
    end
  end

  implications = []
  all_stmts.each do |big_arr|
    for row in 0...big_arr.count-1
      tmp_arr = big_arr[row]
      for other_row in row+1...big_arr.count
        other_tmp_arr = big_arr[other_row]
        for col in 0...tmp_arr.count
          stmt = tmp_arr[col]
          other_stmt = other_tmp_arr[col]
          implications << "~#{stmt}|~#{other_stmt}"
        end
      end
    end
  end

  all_stmts.flatten!(1)
  all_stmts.each do |tmp_arr|
    for col in 0...tmp_arr.count-1
      for other_col in col+1...tmp_arr.count
        stmt = tmp_arr[col]
        other_stmt = tmp_arr[other_col]
        implications << "~#{stmt}|~#{other_stmt}"
      end
    end
  end

  for row in 0...cats.count-1
    cat = cats[row]
    for next_row in row+1...cats.count
      next_cat = cats[next_row]
      for third_row in next_row+1...cats.count
        third_cat = cats[third_row]
        for left_col in 0...cat.count
          left_val = cat[left_col]
          for right_col in 0...next_cat.count
            right_val = next_cat[right_col]
            for third_col in 0...third_cat.count
              third_val = third_cat[third_col] 
              implications << "~#{left_val}_#{third_val}|~#{right_val}_#{third_val}|#{left_val}_#{right_val}"
            end
          end
        end
      end
    end
  end

  for row in 0...cats.count-1
    cat = cats[row]
    for next_row in row+1...cats.count
      next_cat = cats[next_row]
      for col in 0...next_cat.count
        tmp_arr = []
        for other_col in 0...next_cat.count
          tmp_arr << "#{cat[col]}_#{next_cat[other_col]}"
        end
        implications << tmp_arr.join("|")
      end
    end
  end
  for row in 0...cats.count-1
    cat = cats[row]
    for next_row in row+1...cats.count
      next_cat = cats[next_row]
      for col in 0...next_cat.count
        tmp_arr = []
        for other_col in 0...next_cat.count
          tmp_arr << "#{cat[other_col]}_#{next_cat[col]}"
        end
        implications << tmp_arr.join("|")
      end
    end
  end

  puts implications.join("\n")
end
