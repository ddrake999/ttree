# This example takes about 1 minute to run on my machine
# Be sure to specify options -p -f (-g helps a little too)

# DEFINITIONS
# -----------
# Baseball Cards
# 55
# 65
# 72
# 79  

# First Names
# Josiah            jos
# Kaitlyn           kai
# Macie             mac
# Madison           mad

# Candy Bars
# Baby Ruth         bab
# Snickers          sni
# Three Musketeers  thr
# Twix              twi

# Cheeses
# blue              blu
# feta              fet
# havarti           hav
# ricotta           ric

# SYMBOLS
# -------
# 55 65 72 79  
# jos kai mac mad
# bab sni thr twi
# blu fet hav ric

# There are 4*4*3*2 = 96 possible pairings

# CLUES
# -----
# The person who bought the Twix has fewer baseball cards than the person who bought the Three Musketeers.
# The feta enthusiast is Kaitlyn.
# Of Madison and the person who bought the Snickers, one has 55 baseball cards and the other loves blue cheese.
# Macie has fewer baseball cards than the feta enthusiast.
# The collector who has 72 baseball cards loves feta.
# The ricotta enthusiast didn't buy the Three Musketeers.
# Either the collector who has 65 baseball cards or the collector who has 72 baseball cards bought the Snickers.
# The havarti enthusiast didn't buy the Three Musketeers and is not Josiah.
# The person who bought the Baby Ruth is not Madison.

# TRANSLATED CLUES
# ----------------
~79_twi
~55_thr
~65_twi | 72_thr | 79_thr
~72_twi | 79_thr
~72_thr | 55_twi | 65_twi
~65_thr | 55_twi
kai_fet
~mad_sni
~55_blu
55_mad | mad_blu
sni_blu | 55_sni
~79_mac
~mac_fet
~55_fet
~65_mac | 72_fet | 79_fet 
~72_mac | 79_fet
~72_fet | 55_mac | 65_mac
~65_fet | 55_mac
72_fet
~thr_ric
~55_sni & ~79_sni
~thr_hav & ~jos_hav
~mad_bab



# GENERATED RULES (Used sampgen.rb with defn1.txt)
# ------------------------------------------------
~55_jos|~65_jos
~55_kai|~65_kai
~55_mac|~65_mac
~55_mad|~65_mad
~55_jos|~72_jos
~55_kai|~72_kai
~55_mac|~72_mac
~55_mad|~72_mad
~55_jos|~79_jos
~55_kai|~79_kai
~55_mac|~79_mac
~55_mad|~79_mad
~65_jos|~72_jos
~65_kai|~72_kai
~65_mac|~72_mac
~65_mad|~72_mad
~65_jos|~79_jos
~65_kai|~79_kai
~65_mac|~79_mac
~65_mad|~79_mad
~72_jos|~79_jos
~72_kai|~79_kai
~72_mac|~79_mac
~72_mad|~79_mad
~55_bab|~65_bab
~55_sni|~65_sni
~55_thr|~65_thr
~55_twi|~65_twi
~55_bab|~72_bab
~55_sni|~72_sni
~55_thr|~72_thr
~55_twi|~72_twi
~55_bab|~79_bab
~55_sni|~79_sni
~55_thr|~79_thr
~55_twi|~79_twi
~65_bab|~72_bab
~65_sni|~72_sni
~65_thr|~72_thr
~65_twi|~72_twi
~65_bab|~79_bab
~65_sni|~79_sni
~65_thr|~79_thr
~65_twi|~79_twi
~72_bab|~79_bab
~72_sni|~79_sni
~72_thr|~79_thr
~72_twi|~79_twi
~55_blu|~65_blu
~55_fet|~65_fet
~55_hav|~65_hav
~55_ric|~65_ric
~55_blu|~72_blu
~55_fet|~72_fet
~55_hav|~72_hav
~55_ric|~72_ric
~55_blu|~79_blu
~55_fet|~79_fet
~55_hav|~79_hav
~55_ric|~79_ric
~65_blu|~72_blu
~65_fet|~72_fet
~65_hav|~72_hav
~65_ric|~72_ric
~65_blu|~79_blu
~65_fet|~79_fet
~65_hav|~79_hav
~65_ric|~79_ric
~72_blu|~79_blu
~72_fet|~79_fet
~72_hav|~79_hav
~72_ric|~79_ric
~jos_bab|~kai_bab
~jos_sni|~kai_sni
~jos_thr|~kai_thr
~jos_twi|~kai_twi
~jos_bab|~mac_bab
~jos_sni|~mac_sni
~jos_thr|~mac_thr
~jos_twi|~mac_twi
~jos_bab|~mad_bab
~jos_sni|~mad_sni
~jos_thr|~mad_thr
~jos_twi|~mad_twi
~kai_bab|~mac_bab
~kai_sni|~mac_sni
~kai_thr|~mac_thr
~kai_twi|~mac_twi
~kai_bab|~mad_bab
~kai_sni|~mad_sni
~kai_thr|~mad_thr
~kai_twi|~mad_twi
~mac_bab|~mad_bab
~mac_sni|~mad_sni
~mac_thr|~mad_thr
~mac_twi|~mad_twi
~jos_blu|~kai_blu
~jos_fet|~kai_fet
~jos_hav|~kai_hav
~jos_ric|~kai_ric
~jos_blu|~mac_blu
~jos_fet|~mac_fet
~jos_hav|~mac_hav
~jos_ric|~mac_ric
~jos_blu|~mad_blu
~jos_fet|~mad_fet
~jos_hav|~mad_hav
~jos_ric|~mad_ric
~kai_blu|~mac_blu
~kai_fet|~mac_fet
~kai_hav|~mac_hav
~kai_ric|~mac_ric
~kai_blu|~mad_blu
~kai_fet|~mad_fet
~kai_hav|~mad_hav
~kai_ric|~mad_ric
~mac_blu|~mad_blu
~mac_fet|~mad_fet
~mac_hav|~mad_hav
~mac_ric|~mad_ric
~bab_blu|~sni_blu
~bab_fet|~sni_fet
~bab_hav|~sni_hav
~bab_ric|~sni_ric
~bab_blu|~thr_blu
~bab_fet|~thr_fet
~bab_hav|~thr_hav
~bab_ric|~thr_ric
~bab_blu|~twi_blu
~bab_fet|~twi_fet
~bab_hav|~twi_hav
~bab_ric|~twi_ric
~sni_blu|~thr_blu
~sni_fet|~thr_fet
~sni_hav|~thr_hav
~sni_ric|~thr_ric
~sni_blu|~twi_blu
~sni_fet|~twi_fet
~sni_hav|~twi_hav
~sni_ric|~twi_ric
~thr_blu|~twi_blu
~thr_fet|~twi_fet
~thr_hav|~twi_hav
~thr_ric|~twi_ric
~55_jos|~55_kai
~55_jos|~55_mac
~55_jos|~55_mad
~55_kai|~55_mac
~55_kai|~55_mad
~55_mac|~55_mad
~65_jos|~65_kai
~65_jos|~65_mac
~65_jos|~65_mad
~65_kai|~65_mac
~65_kai|~65_mad
~65_mac|~65_mad
~72_jos|~72_kai
~72_jos|~72_mac
~72_jos|~72_mad
~72_kai|~72_mac
~72_kai|~72_mad
~72_mac|~72_mad
~79_jos|~79_kai
~79_jos|~79_mac
~79_jos|~79_mad
~79_kai|~79_mac
~79_kai|~79_mad
~79_mac|~79_mad
~55_bab|~55_sni
~55_bab|~55_thr
~55_bab|~55_twi
~55_sni|~55_thr
~55_sni|~55_twi
~55_thr|~55_twi
~65_bab|~65_sni
~65_bab|~65_thr
~65_bab|~65_twi
~65_sni|~65_thr
~65_sni|~65_twi
~65_thr|~65_twi
~72_bab|~72_sni
~72_bab|~72_thr
~72_bab|~72_twi
~72_sni|~72_thr
~72_sni|~72_twi
~72_thr|~72_twi
~79_bab|~79_sni
~79_bab|~79_thr
~79_bab|~79_twi
~79_sni|~79_thr
~79_sni|~79_twi
~79_thr|~79_twi
~55_blu|~55_fet
~55_blu|~55_hav
~55_blu|~55_ric
~55_fet|~55_hav
~55_fet|~55_ric
~55_hav|~55_ric
~65_blu|~65_fet
~65_blu|~65_hav
~65_blu|~65_ric
~65_fet|~65_hav
~65_fet|~65_ric
~65_hav|~65_ric
~72_blu|~72_fet
~72_blu|~72_hav
~72_blu|~72_ric
~72_fet|~72_hav
~72_fet|~72_ric
~72_hav|~72_ric
~79_blu|~79_fet
~79_blu|~79_hav
~79_blu|~79_ric
~79_fet|~79_hav
~79_fet|~79_ric
~79_hav|~79_ric
~jos_bab|~jos_sni
~jos_bab|~jos_thr
~jos_bab|~jos_twi
~jos_sni|~jos_thr
~jos_sni|~jos_twi
~jos_thr|~jos_twi
~kai_bab|~kai_sni
~kai_bab|~kai_thr
~kai_bab|~kai_twi
~kai_sni|~kai_thr
~kai_sni|~kai_twi
~kai_thr|~kai_twi
~mac_bab|~mac_sni
~mac_bab|~mac_thr
~mac_bab|~mac_twi
~mac_sni|~mac_thr
~mac_sni|~mac_twi
~mac_thr|~mac_twi
~mad_bab|~mad_sni
~mad_bab|~mad_thr
~mad_bab|~mad_twi
~mad_sni|~mad_thr
~mad_sni|~mad_twi
~mad_thr|~mad_twi
~jos_blu|~jos_fet
~jos_blu|~jos_hav
~jos_blu|~jos_ric
~jos_fet|~jos_hav
~jos_fet|~jos_ric
~jos_hav|~jos_ric
~kai_blu|~kai_fet
~kai_blu|~kai_hav
~kai_blu|~kai_ric
~kai_fet|~kai_hav
~kai_fet|~kai_ric
~kai_hav|~kai_ric
~mac_blu|~mac_fet
~mac_blu|~mac_hav
~mac_blu|~mac_ric
~mac_fet|~mac_hav
~mac_fet|~mac_ric
~mac_hav|~mac_ric
~mad_blu|~mad_fet
~mad_blu|~mad_hav
~mad_blu|~mad_ric
~mad_fet|~mad_hav
~mad_fet|~mad_ric
~mad_hav|~mad_ric
~bab_blu|~bab_fet
~bab_blu|~bab_hav
~bab_blu|~bab_ric
~bab_fet|~bab_hav
~bab_fet|~bab_ric
~bab_hav|~bab_ric
~sni_blu|~sni_fet
~sni_blu|~sni_hav
~sni_blu|~sni_ric
~sni_fet|~sni_hav
~sni_fet|~sni_ric
~sni_hav|~sni_ric
~thr_blu|~thr_fet
~thr_blu|~thr_hav
~thr_blu|~thr_ric
~thr_fet|~thr_hav
~thr_fet|~thr_ric
~thr_hav|~thr_ric
~twi_blu|~twi_fet
~twi_blu|~twi_hav
~twi_blu|~twi_ric
~twi_fet|~twi_hav
~twi_fet|~twi_ric
~twi_hav|~twi_ric
~55_bab|~jos_bab|55_jos
~55_sni|~jos_sni|55_jos
~55_thr|~jos_thr|55_jos
~55_twi|~jos_twi|55_jos
~55_bab|~kai_bab|55_kai
~55_sni|~kai_sni|55_kai
~55_thr|~kai_thr|55_kai
~55_twi|~kai_twi|55_kai
~55_bab|~mac_bab|55_mac
~55_sni|~mac_sni|55_mac
~55_thr|~mac_thr|55_mac
~55_twi|~mac_twi|55_mac
~55_bab|~mad_bab|55_mad
~55_sni|~mad_sni|55_mad
~55_thr|~mad_thr|55_mad
~55_twi|~mad_twi|55_mad
~65_bab|~jos_bab|65_jos
~65_sni|~jos_sni|65_jos
~65_thr|~jos_thr|65_jos
~65_twi|~jos_twi|65_jos
~65_bab|~kai_bab|65_kai
~65_sni|~kai_sni|65_kai
~65_thr|~kai_thr|65_kai
~65_twi|~kai_twi|65_kai
~65_bab|~mac_bab|65_mac
~65_sni|~mac_sni|65_mac
~65_thr|~mac_thr|65_mac
~65_twi|~mac_twi|65_mac
~65_bab|~mad_bab|65_mad
~65_sni|~mad_sni|65_mad
~65_thr|~mad_thr|65_mad
~65_twi|~mad_twi|65_mad
~72_bab|~jos_bab|72_jos
~72_sni|~jos_sni|72_jos
~72_thr|~jos_thr|72_jos
~72_twi|~jos_twi|72_jos
~72_bab|~kai_bab|72_kai
~72_sni|~kai_sni|72_kai
~72_thr|~kai_thr|72_kai
~72_twi|~kai_twi|72_kai
~72_bab|~mac_bab|72_mac
~72_sni|~mac_sni|72_mac
~72_thr|~mac_thr|72_mac
~72_twi|~mac_twi|72_mac
~72_bab|~mad_bab|72_mad
~72_sni|~mad_sni|72_mad
~72_thr|~mad_thr|72_mad
~72_twi|~mad_twi|72_mad
~79_bab|~jos_bab|79_jos
~79_sni|~jos_sni|79_jos
~79_thr|~jos_thr|79_jos
~79_twi|~jos_twi|79_jos
~79_bab|~kai_bab|79_kai
~79_sni|~kai_sni|79_kai
~79_thr|~kai_thr|79_kai
~79_twi|~kai_twi|79_kai
~79_bab|~mac_bab|79_mac
~79_sni|~mac_sni|79_mac
~79_thr|~mac_thr|79_mac
~79_twi|~mac_twi|79_mac
~79_bab|~mad_bab|79_mad
~79_sni|~mad_sni|79_mad
~79_thr|~mad_thr|79_mad
~79_twi|~mad_twi|79_mad
~55_blu|~jos_blu|55_jos
~55_fet|~jos_fet|55_jos
~55_hav|~jos_hav|55_jos
~55_ric|~jos_ric|55_jos
~55_blu|~kai_blu|55_kai
~55_fet|~kai_fet|55_kai
~55_hav|~kai_hav|55_kai
~55_ric|~kai_ric|55_kai
~55_blu|~mac_blu|55_mac
~55_fet|~mac_fet|55_mac
~55_hav|~mac_hav|55_mac
~55_ric|~mac_ric|55_mac
~55_blu|~mad_blu|55_mad
~55_fet|~mad_fet|55_mad
~55_hav|~mad_hav|55_mad
~55_ric|~mad_ric|55_mad
~65_blu|~jos_blu|65_jos
~65_fet|~jos_fet|65_jos
~65_hav|~jos_hav|65_jos
~65_ric|~jos_ric|65_jos
~65_blu|~kai_blu|65_kai
~65_fet|~kai_fet|65_kai
~65_hav|~kai_hav|65_kai
~65_ric|~kai_ric|65_kai
~65_blu|~mac_blu|65_mac
~65_fet|~mac_fet|65_mac
~65_hav|~mac_hav|65_mac
~65_ric|~mac_ric|65_mac
~65_blu|~mad_blu|65_mad
~65_fet|~mad_fet|65_mad
~65_hav|~mad_hav|65_mad
~65_ric|~mad_ric|65_mad
~72_blu|~jos_blu|72_jos
~72_fet|~jos_fet|72_jos
~72_hav|~jos_hav|72_jos
~72_ric|~jos_ric|72_jos
~72_blu|~kai_blu|72_kai
~72_fet|~kai_fet|72_kai
~72_hav|~kai_hav|72_kai
~72_ric|~kai_ric|72_kai
~72_blu|~mac_blu|72_mac
~72_fet|~mac_fet|72_mac
~72_hav|~mac_hav|72_mac
~72_ric|~mac_ric|72_mac
~72_blu|~mad_blu|72_mad
~72_fet|~mad_fet|72_mad
~72_hav|~mad_hav|72_mad
~72_ric|~mad_ric|72_mad
~79_blu|~jos_blu|79_jos
~79_fet|~jos_fet|79_jos
~79_hav|~jos_hav|79_jos
~79_ric|~jos_ric|79_jos
~79_blu|~kai_blu|79_kai
~79_fet|~kai_fet|79_kai
~79_hav|~kai_hav|79_kai
~79_ric|~kai_ric|79_kai
~79_blu|~mac_blu|79_mac
~79_fet|~mac_fet|79_mac
~79_hav|~mac_hav|79_mac
~79_ric|~mac_ric|79_mac
~79_blu|~mad_blu|79_mad
~79_fet|~mad_fet|79_mad
~79_hav|~mad_hav|79_mad
~79_ric|~mad_ric|79_mad
~55_blu|~bab_blu|55_bab
~55_fet|~bab_fet|55_bab
~55_hav|~bab_hav|55_bab
~55_ric|~bab_ric|55_bab
~55_blu|~sni_blu|55_sni
~55_fet|~sni_fet|55_sni
~55_hav|~sni_hav|55_sni
~55_ric|~sni_ric|55_sni
~55_blu|~thr_blu|55_thr
~55_fet|~thr_fet|55_thr
~55_hav|~thr_hav|55_thr
~55_ric|~thr_ric|55_thr
~55_blu|~twi_blu|55_twi
~55_fet|~twi_fet|55_twi
~55_hav|~twi_hav|55_twi
~55_ric|~twi_ric|55_twi
~65_blu|~bab_blu|65_bab
~65_fet|~bab_fet|65_bab
~65_hav|~bab_hav|65_bab
~65_ric|~bab_ric|65_bab
~65_blu|~sni_blu|65_sni
~65_fet|~sni_fet|65_sni
~65_hav|~sni_hav|65_sni
~65_ric|~sni_ric|65_sni
~65_blu|~thr_blu|65_thr
~65_fet|~thr_fet|65_thr
~65_hav|~thr_hav|65_thr
~65_ric|~thr_ric|65_thr
~65_blu|~twi_blu|65_twi
~65_fet|~twi_fet|65_twi
~65_hav|~twi_hav|65_twi
~65_ric|~twi_ric|65_twi
~72_blu|~bab_blu|72_bab
~72_fet|~bab_fet|72_bab
~72_hav|~bab_hav|72_bab
~72_ric|~bab_ric|72_bab
~72_blu|~sni_blu|72_sni
~72_fet|~sni_fet|72_sni
~72_hav|~sni_hav|72_sni
~72_ric|~sni_ric|72_sni
~72_blu|~thr_blu|72_thr
~72_fet|~thr_fet|72_thr
~72_hav|~thr_hav|72_thr
~72_ric|~thr_ric|72_thr
~72_blu|~twi_blu|72_twi
~72_fet|~twi_fet|72_twi
~72_hav|~twi_hav|72_twi
~72_ric|~twi_ric|72_twi
~79_blu|~bab_blu|79_bab
~79_fet|~bab_fet|79_bab
~79_hav|~bab_hav|79_bab
~79_ric|~bab_ric|79_bab
~79_blu|~sni_blu|79_sni
~79_fet|~sni_fet|79_sni
~79_hav|~sni_hav|79_sni
~79_ric|~sni_ric|79_sni
~79_blu|~thr_blu|79_thr
~79_fet|~thr_fet|79_thr
~79_hav|~thr_hav|79_thr
~79_ric|~thr_ric|79_thr
~79_blu|~twi_blu|79_twi
~79_fet|~twi_fet|79_twi
~79_hav|~twi_hav|79_twi
~79_ric|~twi_ric|79_twi
~jos_blu|~bab_blu|jos_bab
~jos_fet|~bab_fet|jos_bab
~jos_hav|~bab_hav|jos_bab
~jos_ric|~bab_ric|jos_bab
~jos_blu|~sni_blu|jos_sni
~jos_fet|~sni_fet|jos_sni
~jos_hav|~sni_hav|jos_sni
~jos_ric|~sni_ric|jos_sni
~jos_blu|~thr_blu|jos_thr
~jos_fet|~thr_fet|jos_thr
~jos_hav|~thr_hav|jos_thr
~jos_ric|~thr_ric|jos_thr
~jos_blu|~twi_blu|jos_twi
~jos_fet|~twi_fet|jos_twi
~jos_hav|~twi_hav|jos_twi
~jos_ric|~twi_ric|jos_twi
~kai_blu|~bab_blu|kai_bab
~kai_fet|~bab_fet|kai_bab
~kai_hav|~bab_hav|kai_bab
~kai_ric|~bab_ric|kai_bab
~kai_blu|~sni_blu|kai_sni
~kai_fet|~sni_fet|kai_sni
~kai_hav|~sni_hav|kai_sni
~kai_ric|~sni_ric|kai_sni
~kai_blu|~thr_blu|kai_thr
~kai_fet|~thr_fet|kai_thr
~kai_hav|~thr_hav|kai_thr
~kai_ric|~thr_ric|kai_thr
~kai_blu|~twi_blu|kai_twi
~kai_fet|~twi_fet|kai_twi
~kai_hav|~twi_hav|kai_twi
~kai_ric|~twi_ric|kai_twi
~mac_blu|~bab_blu|mac_bab
~mac_fet|~bab_fet|mac_bab
~mac_hav|~bab_hav|mac_bab
~mac_ric|~bab_ric|mac_bab
~mac_blu|~sni_blu|mac_sni
~mac_fet|~sni_fet|mac_sni
~mac_hav|~sni_hav|mac_sni
~mac_ric|~sni_ric|mac_sni
~mac_blu|~thr_blu|mac_thr
~mac_fet|~thr_fet|mac_thr
~mac_hav|~thr_hav|mac_thr
~mac_ric|~thr_ric|mac_thr
~mac_blu|~twi_blu|mac_twi
~mac_fet|~twi_fet|mac_twi
~mac_hav|~twi_hav|mac_twi
~mac_ric|~twi_ric|mac_twi
~mad_blu|~bab_blu|mad_bab
~mad_fet|~bab_fet|mad_bab
~mad_hav|~bab_hav|mad_bab
~mad_ric|~bab_ric|mad_bab
~mad_blu|~sni_blu|mad_sni
~mad_fet|~sni_fet|mad_sni
~mad_hav|~sni_hav|mad_sni
~mad_ric|~sni_ric|mad_sni
~mad_blu|~thr_blu|mad_thr
~mad_fet|~thr_fet|mad_thr
~mad_hav|~thr_hav|mad_thr
~mad_ric|~thr_ric|mad_thr
~mad_blu|~twi_blu|mad_twi
~mad_fet|~twi_fet|mad_twi
~mad_hav|~twi_hav|mad_twi
~mad_ric|~twi_ric|mad_twi
55_jos|55_kai|55_mac|55_mad
65_jos|65_kai|65_mac|65_mad
72_jos|72_kai|72_mac|72_mad
79_jos|79_kai|79_mac|79_mad
55_bab|55_sni|55_thr|55_twi
65_bab|65_sni|65_thr|65_twi
72_bab|72_sni|72_thr|72_twi
79_bab|79_sni|79_thr|79_twi
55_blu|55_fet|55_hav|55_ric
65_blu|65_fet|65_hav|65_ric
72_blu|72_fet|72_hav|72_ric
79_blu|79_fet|79_hav|79_ric
jos_bab|jos_sni|jos_thr|jos_twi
kai_bab|kai_sni|kai_thr|kai_twi
mac_bab|mac_sni|mac_thr|mac_twi
mad_bab|mad_sni|mad_thr|mad_twi
jos_blu|jos_fet|jos_hav|jos_ric
kai_blu|kai_fet|kai_hav|kai_ric
mac_blu|mac_fet|mac_hav|mac_ric
mad_blu|mad_fet|mad_hav|mad_ric
bab_blu|bab_fet|bab_hav|bab_ric
sni_blu|sni_fet|sni_hav|sni_ric
thr_blu|thr_fet|thr_hav|thr_ric
twi_blu|twi_fet|twi_hav|twi_ric
